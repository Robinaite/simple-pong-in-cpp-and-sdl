﻿#pragma once
#include <SDL.h>


enum class PaddlePosition { LEFT,RIGHT };
enum class Direction { NONE,UP,DOWN };

class Paddle
{
public:
	Paddle(PaddlePosition pos);
	void MoveUp();
	void MoveDown();
	void HandleInput(SDL_Event const &event);
	void Update(double deltaTime);
	void Draw(SDL_Renderer *renderer) const;
	double GetXPosition() const;
	PaddlePosition GetPaddlePosition() const;
	double GetYPosition() const;
	double GetPaddleHeight() const;
private:
	SDL_Rect position{};
	PaddlePosition paddlePos;
	Direction nextDirection = Direction::NONE;
	double yPos;
	bool upPressed = false;
	bool downPressed = false;
};
