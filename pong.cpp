﻿#include "pong.hpp"

Pong::Pong() :leftPaddle{PaddlePosition::LEFT},rightPaddle{PaddlePosition::RIGHT}
{
	SDL_CreateWindowAndRenderer(680, 480, SDL_WINDOW_RESIZABLE, &gameWindow, &gameWindowRenderer);
	SDL_RenderSetLogicalSize(gameWindowRenderer, 400, 400);
	ball.Init(gameWindowRenderer,(400 / 2) - 12, (400 / 2) - 12);
}

void Pong::GameLoop()
{
	bool keepRunning = true;                                  
    while(keepRunning)                                        
    {                                                          
        while(SDL_PollEvent(&gameWindowEvent) > 0)         
        {                                                      
            switch(gameWindowEvent.type)                   
            {                                                  
                case SDL_QUIT:                                 
                    keepRunning = false;                      
            }

            leftPaddle.HandleInput(gameWindowEvent);
        	rightPaddle.HandleInput(gameWindowEvent);
        	
        }                                                      

        Update(1.0/60.0);                                      
        Draw();                                                
    }      
}

void Pong::Update(double deltaTime)
{
	leftPaddle.Update(deltaTime);
	rightPaddle.Update(deltaTime);
	ball.Update(deltaTime);
	ball.CheckCollisionWithPaddle(leftPaddle,deltaTime);
	ball.CheckCollisionWithPaddle(rightPaddle,deltaTime);
}

void Pong::Draw()
{
	SDL_RenderClear(gameWindowRenderer);  
    leftPaddle.Draw(gameWindowRenderer);
	rightPaddle.Draw(gameWindowRenderer);
	ball.Draw(gameWindowRenderer);
    SDL_RenderPresent(gameWindowRenderer);
	
}
