#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include "paddle.hpp"

class Ball
{
public:
	Ball() = default;
	~Ball() = default;
	void RandomizeVelocity();
	void Init(SDL_Renderer *renderer,int x, int y);
	void Update(double deltaTime);
	void Draw(SDL_Renderer *renderer);
	void CheckCollisionWithPaddle(Paddle &paddle,const double deltaTime);
private:
	int xVel;
	int yVel;
	double xPos;
	double yPos;
	SDL_Texture *image;
	SDL_Rect position;
};

