﻿#include "ball.hpp"

#include <random>

void Ball::RandomizeVelocity()
{
	std::random_device dev;
	std::mt19937_64 randGenerator(dev());
	const std::uniform_int_distribution<> intDistX(0,1);


	if(intDistX(randGenerator) == 0)
	{
		xVel = -3;
	} else
	{
		xVel = 3;
	}
	const std::uniform_int_distribution<> intDistY(0,3);
	yVel = intDistY(randGenerator);
	if(intDistY(randGenerator))
	{
		yVel = -yVel;
	}
}

void Ball::Init(SDL_Renderer* renderer, int x, int y)
{
	IMG_Init(IMG_INIT_PNG);

	SDL_Surface *imageFIle = IMG_Load("ball.png");

	this->image = SDL_CreateTextureFromSurface(renderer,imageFIle);
	SDL_FreeSurface(imageFIle);
	IMG_Quit();

	xPos = x;
	yPos = y;
	position.x = xPos;
	position.y = yPos;

	SDL_QueryTexture(image,nullptr,nullptr,&position.w,&position.h);

	RandomizeVelocity();
	
}

void Ball::Update(const double deltaTime)
{
	xPos += xVel * deltaTime;
	if(xPos < 0 || xPos + position.w > 400)
	{
		//KO
		xPos = (400 / 2) - 12;
		yPos = (400 / 2) - 12;
		position.x = xPos;
		position.y = yPos;
		RandomizeVelocity();
		return;
	}
	position.x = xPos;

	yPos = yPos + yVel * deltaTime;
	if(yPos < 0 || yPos + position.h > 400)
	{
		yPos -= yVel*deltaTime;
		yVel = -yVel;
	}
	
	position.y = yPos;
}

void Ball::Draw(SDL_Renderer* renderer)
{
	SDL_RenderCopy(renderer,image,nullptr,&position);
}

void Ball::CheckCollisionWithPaddle(Paddle& paddle,const double deltaTime)
{
	int ballLeft, ballRight, ballTop, ballBottom;

	//Calculate the sides of rect A
    ballLeft = position.x;
    ballRight = position.x + position.w;
	ballTop = position.y;
	ballBottom = position.y - position.h;

	if(paddle.GetPaddlePosition() == PaddlePosition::LEFT)
	{
		if(ballLeft < paddle.GetXPosition() && ballTop < paddle.GetYPosition() + paddle.GetPaddleHeight() && ballBottom > paddle.GetYPosition() - paddle.GetPaddleHeight())
		{
			xPos -= xVel * deltaTime;
			xVel = -xVel;
		}
	} else
	{
		if(ballRight > paddle.GetXPosition() && ballTop < paddle.GetYPosition() + paddle.GetPaddleHeight() && ballBottom > paddle.GetYPosition()- paddle.GetPaddleHeight())
		{
			xPos -= xVel * deltaTime;
			xVel = -xVel;
		}
	}
}
