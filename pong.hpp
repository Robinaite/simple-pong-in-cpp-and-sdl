﻿#pragma once

#include <SDL.h>

#include "paddle.hpp"
#include "ball.hpp"

class Pong                                  
{                                           
public:                                     
    Pong();                                 
    ~Pong() = default;                      

    void GameLoop();                       
                   

private:
	void Update(double deltaTime);         
    void Draw();         
    SDL_Window   *gameWindow;            
    SDL_Event     gameWindowEvent;      
    SDL_Renderer *gameWindowRenderer;

	Paddle leftPaddle;
	Paddle rightPaddle;
	Ball ball;
};   