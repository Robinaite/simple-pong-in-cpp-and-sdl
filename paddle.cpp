#include "paddle.hpp"


Paddle::Paddle(const PaddlePosition pos) : paddlePos{pos}, yPos{400/2-50}
{

	if(pos == PaddlePosition::LEFT)
	{
		position.x = 50;
	} else
	{
		position.x = 400-50;
	}
	
	position.y = yPos;
	position.w = 25;
	position.h = 75;
}

void Paddle::MoveUp()
{
	nextDirection = Direction::UP;
	upPressed = true;
}

void Paddle::MoveDown()
{
	nextDirection = Direction::DOWN;
	downPressed = true;
}

void Paddle::HandleInput(SDL_Event const& event)
{
	if(event.type == SDL_KEYDOWN)
	{
		if(paddlePos == PaddlePosition::LEFT)
		{
			if(event.key.keysym.sym == SDLK_w)
			{
				MoveUp();
			} else if(event.key.keysym.sym == SDLK_s)
			{
				MoveDown();
			}
		} else if(paddlePos == PaddlePosition::RIGHT)
		{
			if(event.key.keysym.sym == SDLK_UP)
			{
				MoveUp();
			} else if(event.key.keysym.sym == SDLK_DOWN)
			{
				MoveDown();
			}
		}
	} else if(event.type == SDL_KEYUP)
	{
		if(paddlePos == PaddlePosition::LEFT)
		{
			if(event.key.keysym.sym == SDLK_w)
			{
				upPressed = false;
				if(downPressed)
				{
					MoveDown();
				} else
				{
					nextDirection = Direction::NONE;
				}
			} else if(event.key.keysym.sym == SDLK_s)
			{
				downPressed = false;
				if(upPressed)
				{
					MoveUp();
				} else
				{
					nextDirection = Direction::NONE;
				}
			}
			
		} else if(paddlePos == PaddlePosition::RIGHT)
		{
			if(event.key.keysym.sym == SDLK_UP)
			{
				upPressed = false;
				if(downPressed)
				{
					MoveDown();
				} else
				{
					nextDirection = Direction::NONE;
				}
			} else if(event.key.keysym.sym == SDLK_DOWN)
			{
				downPressed = false;
				if(upPressed)
				{
					MoveUp();
				} else
				{
					nextDirection = Direction::NONE;
				}
			}
		}
	}
}

void Paddle::Update(double deltaTime)
{

	if(nextDirection == Direction::NONE)
	{
		return;
	} 

	if(nextDirection == Direction::UP)
	{
		yPos = yPos - 5.0 * deltaTime;
		if(yPos < 0 || yPos + position.h > 400)
		{
			yPos = yPos + 5.0 * deltaTime;
		}

	} else if(nextDirection == Direction::DOWN)
	{
		yPos = yPos + 5.0 * deltaTime;
		if(yPos < 0 || yPos + position.h > 400)
		{
			yPos = yPos - 5.0 * deltaTime;
		}
	}
	position.y = yPos;
}

void Paddle::Draw(SDL_Renderer *renderer) const
{
	SDL_SetRenderDrawColor(renderer,255,255,255,255);
	SDL_RenderFillRect(renderer, &position);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

double Paddle::GetXPosition() const
{
	return paddlePos == PaddlePosition::LEFT ? position.x + position.w : position.x;
}

PaddlePosition Paddle::GetPaddlePosition() const
{
	return paddlePos;
}

double Paddle::GetYPosition() const
{
	return yPos;
}

double Paddle::GetPaddleHeight() const
{
	return position.h;
}
